const pino = require("pino");
const { join, resolve } = require("path");
const { LOG_LEVEL, PWD } = process.env;
const { name, version } = require("../package.json");
const { format } = require("date-fns");

const today = format(new Date(), "yyyy-MM-dd");
const logPath = PWD ? join(PWD, "logs") : resolve("logs");
const logFile = join(logPath, `${today}.log`);

const transport = pino.transport({
  targets: [
    {
      target: "pino/file",
      options: { destination: logFile },
    },
    { target: "pino-pretty" },
  ],
});

const logger = pino(
  {
    name,
    level: LOG_LEVEL || "info",
    version,
  },
  transport
);

module.exports = logger;


