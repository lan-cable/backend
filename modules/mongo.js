const { MongoClient } = require("mongodb");
// Replace the uri string with your connection string.

if (!process.env.MONGODB_URI) {
  throw new Error("Please add your Mongo URI to .env");
}

const uri = process.env.MONGODB_URI;
const client = new MongoClient(uri);

const db = client.db(process.env.MONGODB_DB || "lan_cable");

module.exports = db;
