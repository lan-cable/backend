const express = require("express");
const app = express();
const logger = require("./logger");
const db = require("./mongo");
app.use(express.json());
const providers = require("./providers");

const players = require("./players");

app.get("/api/players", async (req, res) => {
  const players = await db.collection("players").find({}).toArray();

  // if querry param for history is true, return history for all players
  if (req.query.history) {
    for (const player of players) {
      player.history = await db
        .collection("history")
        .find({ player: player.name })
        .sort({ played_at: -1 })
        .limit(req.query.limit || 5)
        .toArray();
    }
  }

  res.json(players);
});

app.get("/api/players/:player/history", async (req, res) => {
  const player = req.params.player;
  const history = await db
    .collection("history")
    .find({ player: player })
    .toArray();
  res.json(history);
});

app.post("/api/players/:player/skip", async (req, res) => {
  const player = req.params.player;
  logger.info(`${player} skipped`);
  players[player].playNext();

  res.json({ success: true });
});

app.delete("/api/players/:player", async (req, res) => {
  const player = req.params.player;
  db.collection("players").deleteOne({ name: player });
  res.json({ success: true });
});

app.listen(3000, () => {
  logger.info("Server started on port 3000");
});
