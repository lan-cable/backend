const http = require("http");
const io = require("socket.io")();
const Player = require("../classes/player");
const logger = require("./logger");

const httpServer = http.createServer();
io.attach(httpServer);

const players = {};

io.on("connection", (socket) => {
  const name = socket.handshake.auth.token;

  players[name] = new Player(name, socket);

  socket.on("statuschange", (data) => {
    players[name].statusChange(data);
  });

  socket.on("disconnect", () => {
    players[name].disconnect();
    delete players[name];
  });
});

httpServer.listen(process.env.PLAYER_SERVER || 3060);

logger.info("Server started on port 3060");

module.exports = players;
