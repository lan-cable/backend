const axios = require("axios");
class Jellyfin {
  constructor() {
    this.name = "Jellyfin";
    this.url = process.env.JELLYFIN_API_URL;

    this.apiKey = process.env.JELLYFIN_API_KEY;
  }

  getStreamUrl(videoId) {
    return `${this.url}/Videos/${videoId}/stream.mp4?api_key=${this.apiKey}`;
  }

  async getLibraries() {
    try {
      const res = await axios.get(`${this.url}/Items?api_key=${this.apiKey}`);
      return res.data.Items.filter((x) => x.IsFolder).map((x) => ({
        name: x.Name,
        id: x.Id,
      }));
    } catch (error) {
      throw error;
    }
  }

  async getLibraryItems() {
    try {
      const res = await axios.get(
        `${this.url}/Items?Recursive=true&api_key=${this.apiKey}`
      );
      return res.data.Items;
    } catch (error) {
      throw error.response;
    }
  }

  async getRandomVideo() {
    try {
      let items = await this.getLibraryItems();
      items = items.filter((x) => x.MediaType === "Video");
      const randomIndex = Math.floor(Math.random() * items.length);
      return items[randomIndex];
    } catch (error) {
      throw error;
    }
  }
}

module.exports = Jellyfin;
