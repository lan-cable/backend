const providers = require("../modules/providers");
const db = require("../modules/mongo");
const logger = require("../modules/logger");
class Player {
  constructor(name, socket) {
    this.name = name;
    this.socket = socket;
    this.errorTimestamp = null;
    logger.info(`${this.name} connected`);
    db.collection("players").updateOne(
      { name: name },
      { $setOnInsert: { name: name } },
      { upsert: true }
    );
  }

  #play(videoId, provider) {
    this.socket.emit("play", providers[provider].getStreamUrl(videoId));
  }

  #updatePlaying(id, name, series, provider) {
    logger.info(`${this.name} playing ${name}`);
    db.collection("players").updateOne(
      { name: this.name },
      {
        $set: {
          playing: {
            id: id,
            name: name,
            series: series,
            provider: provider,
          },
        },
      }
    );
    db.collection("history").insertOne({
      id: id,
      name: name,
      series: series,
      provider: provider,
      player: this.name,
      played_at: new Date(),
    });
  }

  #updateStats(status) {
    db.collection("players").updateOne(
      { name: this.name },
      {
        $set: {
          state: status.state,
          time: status.time,
          version: status.version,
          aspectratio: status.aspectratio,
          stats: status.stats,
          fullscreen: status.fullscreen,
        },
      }
    );
  }

  #clearState(state) {
    db.collection("players").updateOne(
      { name: this.name },
      {
        $set: {
          state: state,
          stats: null,
          time: null,
          version: null,
          aspectratio: null,
          fullscreen: null,
        },
      }
    );
  }

  async playNext() {
    this.#clearState("loading-next");

    const player = await db.collection("players").findOne({ name: this.name });
    if (player.playListId) {
      const playlist = await db.collection("playlists").findOne({
        id: player.playListId,
      });

      const randomIndex = Math.floor(Math.random() * playlist.videos.length);
      const video = playlist.videos[randomIndex];
      this.#play(video.id, video.provider);
      this.#updatePlaying(video.id, video.name, video.series, video.provider);
      return;
    }

    const defaultProvider = Object.keys(providers)[0];
    const randomVideo = await providers[defaultProvider].getRandomVideo();
    this.#play(randomVideo.Id, defaultProvider);
    this.#updatePlaying(
      randomVideo.Id,
      randomVideo.Name,
      randomVideo.SeriesName,
      defaultProvider
    );
  }

  async statusChange(data) {
    if (data.error) {
      this.#clearState("error");
      if (!this.errorTimestamp) {
        this.errorTimestamp = new Date();
      }
      if (new Date() - this.errorTimestamp > 10000) {
        this.logger.error(`${this.name} error`, data);
        this.errorTimestamp = null;
        await this.playNext();
      }
      return;
    }
    this.errorTimestamp = null;
    this.#updateStats(data.status);

    if (data.status.state !== "playing") {
      this.playNext();
    }
    this.errorTimestamp = null;
  }

  disconnect() {
    this.logger.info(`${this.name} disconnected`);
    this.#clearState("disconnected");
  }
}

module.exports = Player;
